import React from 'react'
import { Container } from '../Styles/layoutStyled'

function Home() {
    return (
        <Container>
            <h1>Home</h1>
            <hr/>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores maxime minima dolore dolores fuga. Error sed explicabo nostrum dignissimos fugit voluptatum quod suscipit perferendis, vero quo repellat libero! Architecto, voluptatem.</p>
        </Container>
    )
}

export default Home
