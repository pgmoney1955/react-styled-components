import { useState } from 'react'
import { Routes, Route } from 'react-router-dom'
import Footer from './Components/Footer'
import NavBar from './Components/Nav'
import About from './Pages/About'
import Blog from './Pages/Blog'
import Home from './Pages/Home'

function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      <NavBar/>
      <Routes>
        <Route path="/" element={<Home/>}/>
        <Route path="/about" element={<About/>}/>
        <Route path="/blog" element={<Blog/>}/>
      </Routes>
      <Footer/>
    </>
  )
}

export default App
