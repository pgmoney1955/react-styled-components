import { NavLink as Link } from "react-router-dom";
import styled from 'styled-components';
import { FaBars } from 'react-icons/fa';

interface ShadowedProps {
    readonly show: boolean;
};

export const NavBar = styled.nav<ShadowedProps>`
    background: #34a0ff;
    min-height: 85px;
    height: ${(props:any) => props.show ? 'auto' : '85px'};
    display: flex;
    justify-content: space-between;
    padding: 0.2rem calc((100vw - 1000px) / 2)
`

export const NavLogo = styled(Link)`
    display: flex;
    align-items: center;
    text-decoration: none;
    font-size: 1.5rem;
    font-weight: bold;
    color: #fff;
    @media screen and (max-width: 768px){
        display: none;
        // padding-left: 15px;
    }
`

export const NavMenu = styled.ul<ShadowedProps>`
    display: flex;
    align-items: center;
    list-style-type: none;
    height: auto;
    @media screen and (max-width: 768px){
        // display: none;
        display: ${(props:any) => props.show ? 'block' : 'none'}
    }
`

export const NavList = styled.li`
    @media screen and (max-width: 768px){
        padding: 2rem 0;
        width: 80%;
    }
`

export const NavLink = styled(Link)`
    color: #fff;
    display: flex;
    align-items: center;
    text-decoration: none;
    padding: 0 1rem;
    cursor: pointer;
    &.active{
        color: #000;
    }
`

export const Bars = styled(FaBars)`
    color: #fff;
    display: none;
    @media screen and (max-width: 768px){
        display: block;
        position: absolute;
        top: 0;
        right: 0;
        transform: translate(-100%, 100%);
        font-size: 1.8rem;
        cursor: pointer;
    }
`

export const FooterCon = styled.footer`
    background-color: #333;
    color: #fff;
    padding: 1rem;
    text-align: center;
    position: fixed;
    bottom: 0;
    width: 100%;
`