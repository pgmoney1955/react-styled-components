import React from 'react'
import { FooterCon } from '../Styles/styledElements'

function Footer() {
    return (
        <FooterCon>
            <p>&copy; 2022. React + Styled Components</p>
        </FooterCon>
    )
}

export default Footer
